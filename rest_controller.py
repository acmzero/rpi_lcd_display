from lcd_interface import write as write_lcd
from flask import Flask, request, jsonify

app = Flask(__name__)

@app.route('/send_message/<string:text>/', methods=['GET'])
def simple_write(text):
    write_lcd(text)
    return "Write: " + text

@app.route('/message', methods=['POST'])
def write_text():
    json = request.get_json()
    print(json)
    text = json['text']
    timeout = json['timeout']
    write_lcd(text, timeout)
    return "Message added to queue"

if __name__== '__main__':
    app.run(debug=True, host='0.0.0.0')
