#!/bin/env python3
import time
import Adafruit_CharLCD as LCD
import threading

# RPi LCD Interface
# From guide:
#     https://microdigisoft.com/interfacing-16x2-lcd-display-with-raspberry-pi/




messages = []
current_message = None

BREAK_TIME = 0.05
MIN_TIMEOUT = 1

def init_lcd():
    lcd_rs = 25
    lcd_en = 24
    lcd_d4 = 23
    lcd_d5 = 17
    lcd_d6 = 18
    lcd_d7 = 22
    lcd_backlight = 2
    lcd_columns = 16
    lcd_rows = 2
    lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows, lcd_backlight)
    return lcd

lcd = init_lcd()

class MainLoop(threading.Thread):
    def __init__(self, thread_name, thread_id):
        threading.Thread.__init__(self)
        self.thread_name = thread_name
        self.thread_id = thread_id

    def run(self):
        print("Initializing main loop")
        while True:
            process_queue()
            time.sleep(MIN_TIMEOUT)

def current_millis():
    return round(time.time() * 1000)

class Message():
    def __init__(self, text, timeout=0):
        self.text = text
        self.timeout = timeout
        self.display_time = None
        # self.display_time = 2021-08-23 18:03.1
        pass
    """
        Return how much time has passed since the message is shown in the display,
        if the message is not displayed yet, returns None
    """
    def calculate_display_time(self):
        if self.display_time:
            return current_millis() - self.display_time
        return None

    """
        Return the remaining time of the message in the lcd 
    """
    def calculate_time_to_expire(self):
        return self.timeout - (self.calculate_display_time() / 1000)

    """
        Returns True if the message is expired, otherwise, False
    """
    def is_expired(self):
        return self.calculate_display_time() > self.timeout

    """
        Saves the time when the message is shown in the display
    """
    def displayed(self):
        self.display_time = current_millis()
        return

"""
    Takes the next message in the queue and displays it.
    If the queue is empty nothing is done.
"""
def display_next_queue():
    if len(messages) == 0:
        return
    lcd.clear()
    m = messages.pop(0)
    lcd.message(m.text)
    m.displayed()
    current_message = m

"""
    Display next message if current message is expired.
"""
def process_queue():
    if current_message and not current_message.is_expired():
        time.sleep(current_message.calculate_time_to_expire() + BREAK_TIME)
    else:
        display_next_queue()
    return

"""
    Adds a new message to the last position of the queue.
"""
def add_queue(message):
    messages.append(message)


"""
Displays a message lasting the specified timeout seconds, if timeout less or equals zero,
it will display the message until the function is called again
"""
def write(text, timeout=0):
    message = Message(text,timeout)
    add_queue(message)

main_loop = MainLoop("Main Loop", 1000)
main_loop.start()
